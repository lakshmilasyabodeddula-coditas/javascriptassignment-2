//get and set attributes
const link=document.querySelector("a");
console.log(link.getAttribute("href"));
const inputElement=document.querySelector(".todo-form input");
console.log(inputElement.getAttribute("type"));

link.setAttribute("href","www.google.com");
console.log(link.getAttribute("href"));
