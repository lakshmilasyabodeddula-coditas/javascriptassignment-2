//event 
//event is a responsive input provided by web browser
//click is the most used event
// three ways to add an event
//onclick is added in html  
const btn=document.querySelector(".btn-headline");
console.dir(btn);
btn.onclick=function()
{
    console.log("you clicked me");
}
//method -- addEventListener
btn.addEventListener("click",clickMe());
function clickMe()
{

console.log("you clicked me");
}

//arrow function is shorthand for normal function
