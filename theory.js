// compilation:
console.log(this);
console.log(window);
console.log(firstName);
var firstName = "Lakshmi";
console.log(firstName);
// hoisting
console.log(this);
console.log(window);
console.log(myFunction);
console.log(fullName);
function myFunction(){
    console.log("My name is Laskhmi");
}
var firstName = "Lakshmi";
var lastName = "Lasya"
var fullName = firstName + " " + lastName;
console.log(fullName);
let firstName="Lakshmi";
let lastName="Lasya";
const myFunction1=function(){
    let var1="First Variable";
    let var2="second Variable";
    console.log(var1);
    console.log(var2);
}
//Function execution Context
let string1="dog";
console.log(string1);
function getFullName(firstName,lastName){
    console.log(arguments);
    let myVar="var inside func";
    console.log(myVar);
    const fullName=firstName+" "+lastName;
    return fullName;
}
const personName=getFullName("Lakshmi","Lasya");
console.log(personName);
// lexical environment, scope chain
const lastName = "La";
const printedName = function(){
    const firstName = "Lakshmi";
    function myFunction2(){
        console.log(firstName);
        console.log(lastName);
    }
    myFunction2()
}
printedName();

function printFullName(firstName, lastName){
    function printName(){
        console.log(firstName, lastName);
    }
    return printName;
}
const answer = printFullName("Lakshmi", "lasya");
answer();

function hello(x){
    const a  = "varA";
    const b = "varB";
    return function(){
        console.log(a,b,x);
    }
}
const ans = hello("arg");
ans();
function func(){
    let counter = 0;
    return function(){
        if(counter < 1){
            console.log("Hi You Called me");
            counter++;
        }else{
            console.log("You Already Called me");
        }
    }
}
const myFunc = func();
myFunc();
myFunc();
